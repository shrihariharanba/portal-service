FROM java:8
EXPOSE 8083
COPY portal-rest/target/portal-rest-1.0.jar /data/portal-rest-1.0.jar
CMD java -jar /data/portal-rest-1.0.jar