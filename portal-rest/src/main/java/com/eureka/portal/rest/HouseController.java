package com.eureka.portal.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eureka.portal.infra.model.HouseDetails;
import com.eureka.portal.service.HouseService;

@RestController
@RequestMapping(path = "/house")
public class HouseController {
	
	@Autowired
	HouseService service;
	
	@GetMapping(path="/{houseNo}", produces = "application/json")
    public HouseDetails getHouseDetails(@PathVariable String houseNo)
    {
		return service.getHouseDetails();
    }

}
