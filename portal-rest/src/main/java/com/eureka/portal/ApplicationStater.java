package com.eureka.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.eureka"})
public class ApplicationStater {
	
	public static void main(String[] args) {
		SpringApplication.run(ApplicationStater.class, args);
	}
}
