package com.eureka.portal.service;

import org.springframework.stereotype.Service;

import com.eureka.portal.infra.model.HouseDetails;

@Service
public class HouseService {

	/**
	 * Method gets the House detailscd
	 * @return
	 */
	public HouseDetails getHouseDetails() {
		return new HouseDetails("75/2","Lakshmi Illam","Kuberan 1st Street, Sorgam","Venkat",0000000000);
	}
}
