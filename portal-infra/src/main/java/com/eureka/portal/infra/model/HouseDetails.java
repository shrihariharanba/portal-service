package com.eureka.portal.infra.model;

public class HouseDetails {

	private String houseNo;
	private String houseName;
	private String houseAddress;
	private String ownerName;
	private Integer ownerContactNo;
	
	public HouseDetails(String houseNo, String houseName, String houseAddress, String ownerName,
			Integer ownerContactNo) {
		this.houseNo = houseNo;
		this.houseName = houseName;
		this.houseAddress = houseAddress;
		this.ownerName = ownerName;
		this.ownerContactNo = ownerContactNo;
	}
	
	public String getHouseNo() {
		return houseNo;
	}
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public String getHouseAddress() {
		return houseAddress;
	}
	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public Integer getOwnerContactNo() {
		return ownerContactNo;
	}
	public void setOwnerContactNo(Integer ownerContactNo) {
		this.ownerContactNo = ownerContactNo;
	}
	
}
